from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.core.window import Window
from kivy.app import App


class Gamescreen(BoxLayout):
    def __init__(self, **kwargs):
        super(Gamescreen, self).__init__(**kwargs)

        # Grid Jeu
        self.grid_jeu = FloatLayout()
        self.add_widget(self.grid_jeu)  # Ajoutez self.grid_jeu directement à l'instance de Game

        Window.clearcolor = (1, 1, 1, 1)

        self.mario = Image(source="mario.jpg")
        self.mario.size_hint_x = 0.2
        self.pos_x_mario = 325
        self.pos_y_mario = 125
        self.mario.pos = self.pos_x_mario, self.pos_y_mario
        self.grid_jeu.add_widget(self.mario)

        # Grid boutons
        self.grid_boutons = FloatLayout()
        self.grid_boutons.size_hint_y = 0.18

        # Boutons dans Grid Boutons
        self.Img1 = Image(source="fond2.jpg", allow_stretch=True, keep_ratio=False, nocache=True)
        self.Img1.size_hint_x = 102
        self.grid_boutons.add_widget(self.Img1)
        self.bouton_haut()
        self.bouton_bas()

        self.bouton_gauche()
        self.bouton_droit()
        self.bouton_accueil()

        # Insertion des Grid dans la box principale
        self.add_widget(self.grid_boutons)

    def bouton_haut(self):
        btn_haut = Button(text="HAUT", bold=True)
        btn_haut.size_hint_y = 0.5
        btn_haut.size_hint_x = 0.2
        btn_haut.pos = 350, 53
        btn_haut.background_color = "blue"
        btn_haut.bind(on_press=self.on_press_btn_haut)
        self.grid_boutons.add_widget(btn_haut)

    def bouton_bas(self):
        btn_bas = Button(text="BAS", bold=True)
        btn_bas.size_hint_y = .5
        btn_bas.size_hint_x = 0.2
        btn_bas.pos = 350, 0
        btn_bas.background_color = "blue"
        btn_bas.bind(on_press=self.on_press_btn_bas)
        self.grid_boutons.add_widget(btn_bas)

    def bouton_gauche(self):
        btn_gauche = Button(text="GAUCHE", bold=True)
        btn_gauche.size_hint_y = 0.5
        btn_gauche.size_hint_x = 0.2
        btn_gauche.pos = 270, 25
        btn_gauche.background_color = "blue"
        btn_gauche.bind(on_press=self.on_press_btn_gauche)
        self.grid_boutons.add_widget(btn_gauche)

    def bouton_droit(self):
        btn_droit = Button(text="DROITE", bold=True)
        btn_droit.size_hint_y = 0.5
        btn_droit.size_hint_x = 0.2
        btn_droit.pos = 430, 25
        btn_droit.background_color = "blue"
        btn_droit.bind(on_press=self.on_press_btn_droit)
        self.grid_boutons.add_widget(btn_droit)

    def bouton_accueil(self):
        btn_exit = Button(text="Accueil", bold=True, background_color="red")
        btn_exit.size_hint_x = 0.2
        btn_exit.size_hint_y = 0.2
        btn_exit.bind(on_press=self.on_press_btn_accueil)
        self.grid_boutons.add_widget(btn_exit)

    def on_press_btn_haut(self, instance):
        self.pos_y_mario += 10
        self.mario.pos = self.pos_x_mario, self.pos_y_mario

    def on_press_btn_bas(self, instance):
        self.pos_y_mario -= 10
        self.mario.pos = self.pos_x_mario, self.pos_y_mario

    def on_press_btn_gauche(self, instance):
        self.pos_x_mario -= 10
        self.mario.pos = self.pos_x_mario, self.pos_y_mario

    def on_press_btn_droit(self, instance):
        self.pos_x_mario += 10
        self.mario.pos = self.pos_x_mario, self.pos_y_mario

    def on_press_btn_accueil(self, instance):
        # # if self.parent:
        # self.parent.current = "user"
        self.controller.switch_screen("user")
class GameApp(App):
    def build(self):
        return Gamescreen()

if __name__ == "__main__":
    GameApp().run()
