from gamescreen import Gamescreen
import player
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from player import UserController

class UserScreen(Screen):
    def __init__(self, **kwargs):
        super(UserScreen, self).__init__(**kwargs)
        self.add_widget(UserController().view)

# Définir l'écran de jeu
class GameScreen(Screen):
    def __init__(self, **kwargs):
        super(GameScreen, self).__init__(**kwargs)
        self.add_widget(Gamescreen())



# Définir le gestionnaire d'écrans
class MyScreenManager(ScreenManager):
    def __init__(self, **kwargs):
        super(MyScreenManager, self).__init__(**kwargs)
        self.add_widget(UserScreen(name='user'))
        self.add_widget(GameScreen(name='game'))

# Définir l'application principale
class MyApp(App):
    def build(self):
        return MyScreenManager()

if __name__ == "__main__":
    MyApp().run()



