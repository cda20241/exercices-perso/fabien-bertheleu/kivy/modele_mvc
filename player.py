from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.screenmanager import ScreenManager, Screen

from gamescreen import Gamescreen


class UserModel():
    def __init__(self):
        self.nom = ""
        self.prenom = ""

class UserView(BoxLayout):
    def __init__(self, controller, **kwargs):
        super(UserView, self).__init__(**kwargs)
        # la relation entre controler et vue est bidirectionnelle, la vue doit connaître son controler
        # pour pouvoir interagir avec
        self.controller = controller

        self.orientation = "vertical"
        self.label = Label(text="Utilisateur : "+self.controller.model.prenom+" "+self.controller.model.nom, color="black")
        self.input_prenom = TextInput(text="Prénom")
        self.input_nom = TextInput(text="Nom")
        self.submit_button = Button(text="Valider", on_press=self.on_submit)
        # if self.submit_button.text == "Jouer":
        #     self.submit_button.bind(on_press=self.switch_ecran_jeu)
        #
        # else:
        self.submit_button.bind(on_press=self.bouton_jouer)
        self.submit_button.bind(on_press=self.bouton_jouer_vert)


        # self.submit_button.bind(on_press=self.jouer)

        self.add_widget(self.label)
        self.add_widget(self.input_prenom)
        self.add_widget(self.input_nom)
        self.add_widget(self.submit_button)

    def on_submit(self, instance):
        # l'utilisateur a interagit avec la vue, la vue notifie alors le controler
        self.controller.update_data(self.input_prenom.text, self.input_nom.text)



    def update_label(self, prenom, nom):
        # le controler peut mettre à jour la vue par cette méthode
        self.label.text = " Bienvenue " + prenom + " " + nom

    def bouton_jouer(self, button):
        self.submit_button.text = "Jouer"

    def bouton_jouer_vert(self, button):
        self.submit_button.background_color = "lime"
        self.controller.switch_screen("game")

    # def switch_ecran_jeu(self, button):
    #     self.controller.switch_screen("game")


class UserController():
    def __init__(self):
        # le controler crée la vue et le model
        self.model = UserModel()
        # quand on crée la vue, on lui donne en paramètre l'objet self qui est le controller
        # puisque la vue doit connaître le controller
        self.view = UserView(self)

    def update_data(self, prenom, nom):
        self.model.prenom = prenom
        self.model.nom = nom
        self.view.update_label(prenom, nom)

    def switch_screen(self, screen_name):
        App.get_running_app().root.current = screen_name



class MyApp(App):
    def build(self):
        controller = UserController()
        return controller.view

if __name__ == "__main__":
    MyApp().run()
